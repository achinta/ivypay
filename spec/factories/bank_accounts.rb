# == Schema Information
#
# Table name: bank_accounts
#
#  id            :integer          not null, primary key
#  account_no    :string
#  bank_code1    :string
#  code1_type    :string
#  bank_code2    :string
#  code2_type    :string
#  bank_location :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  bank_id       :integer          not null
#  partner_id    :integer          not null
#
# Indexes
#
#  index_bank_accounts_on_bank_id     (bank_id)
#  index_bank_accounts_on_partner_id  (partner_id)
#

FactoryGirl.define do
  factory :bank_account do
    account_no ""
bank_code1 ""
code1_type ""
bank_code2 ""
code2_type ""
bank_location "MyString"
  end

end
