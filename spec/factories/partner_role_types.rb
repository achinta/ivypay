# == Schema Information
#
# Table name: partner_role_types
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

FactoryGirl.define do
  factory :partner_role_type do
    code "MyString"
name "MyString"
  end

end
