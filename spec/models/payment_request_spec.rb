# == Schema Information
#
# Table name: payment_requests
#
#  id                 :integer          not null, primary key
#  order_no           :string           not null
#  umrn               :string           not null
#  txn_date           :date             not null
#  file_name          :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  amount_total       :decimal(10, 2)
#  status             :string
#  payment_mandate_id :integer
#
# Indexes
#
#  index_payment_requests_on_payment_mandate_id  (payment_mandate_id)
#

require 'rails_helper'

RSpec.describe PaymentRequest, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
