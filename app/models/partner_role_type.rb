# == Schema Information
#
# Table name: partner_role_types
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PartnerRoleType < ActiveRecord::Base
  has_many :partners
end
