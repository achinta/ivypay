# == Schema Information
#
# Table name: partners
#
#  id                   :integer          not null, primary key
#  name                 :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  partner_role_type_id :integer
#  mobile_1             :string
#
# Indexes
#
#  index_partners_on_partner_role_type_id  (partner_role_type_id)
#

class Partner < ActiveRecord::Base
  belongs_to :partner_role_type
  has_many :bank_accounts
end
