# == Schema Information
#
# Table name: payment_requests
#
#  id                 :integer          not null, primary key
#  order_no           :string           not null
#  umrn               :string           not null
#  txn_date           :date             not null
#  file_name          :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  amount_total       :decimal(10, 2)
#  status             :string
#  payment_mandate_id :integer
#
# Indexes
#
#  index_payment_requests_on_payment_mandate_id  (payment_mandate_id)
#

class PaymentRequest < ActiveRecord::Base
  belongs_to :payment_mandate

  before_validation :update_mandate_id
  after_create :send_notifications


  def update_mandate_id
    mandate = PaymentMandate.find_by(mandate_no: self.umrn)

    if mandate then
      self.payment_mandate_id = mandate.id
    end

  end

  def submit
    if self.status == 'submitted' then
      #return True
    end

    self.update(status: 'submitted')
    msg = "Payment of #{self.amount_total} Rs  for Order No - #{self.order_no} submitted to bank"
    if self.payment_mandate_id then
      mobile_nos = self.payment_mandate.payer_mobile_no ? self.payment_mandate.payer_mobile_no : '  '
      mobile_nos = self.payment_mandate.payee_mobile_no ? mobile_nos + ',' +  self.payment_mandate.payee_mobile_no : mobile_nos 
      mobile_nos.gsub(' ','') 
      send_sms(mobile_nos,msg)
    end
  end

  def send_notifications
    #if mandate.present?
    if self.payment_mandate_id then 
      mobile_nos = self.payment_mandate.payer_mobile_no
      msg = "Payment of #{amount_total} Rs initiated for Order No - #{order_no}"
      send_sms(mobile_nos,msg)
    end
  end

  def send_sms(mobile_nos, msg)
    p mobile_nos
    msg_gateway = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx"
    userid = "ajaykumar.c"
    password = "984332"

    sms = URI::escape("Hello test sms #{msg}")
    #Promotional SMS
    #url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=#{userid}&pwd=#{password}&to=#{mobile_nos}&sid=WEBSMS&msg=#{sms}&fl=0" 
   
    #Transactional SMS
    url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=#{userid}&pwd=#{password}&to=#{mobile_nos}&sid=WEBSMS&msg=#{sms}&fl=0&gwid=2" 
    response = Typhoeus.get(url)
    p response
  end

end
