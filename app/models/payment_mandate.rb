# == Schema Information
#
# Table name: payment_mandates
#
#  id                   :integer          not null, primary key
#  mandate_no           :string           not null
#  mandate_status       :string
#  start_date           :date             not null
#  end_date             :date
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  payer_name           :string           not null
#  payer_acct_no        :string           not null
#  payer_bank_code      :string           not null
#  payer_bank_code_type :string           not null
#  payee_name           :string           not null
#  payee_acct_no        :string           not null
#  payee_bank_code      :string           not null
#  payee_bank_code_type :string           not null
#  payee_mobile_no      :string
#  payee_id             :integer
#  payer_id             :integer
#  payer_mobile_no      :string
#
# Indexes
#
#  index_payment_mandates_on_payee_id  (payee_id)
#  index_payment_mandates_on_payer_id  (payer_id)
#

class PaymentMandate < ActiveRecord::Base
end
