json.array!(@payment_mandates) do |payment_mandate|
  json.extract! payment_mandate, :id, :mandate_no, :mandate_status, :start_date, :end_date
  json.url payment_mandate_url(payment_mandate, format: :json)
end
