json.array!(@partner_role_types) do |partner_role_type|
  json.extract! partner_role_type, :id, :code, :name
  json.url partner_role_type_url(partner_role_type, format: :json)
end
