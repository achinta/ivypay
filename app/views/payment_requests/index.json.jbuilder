json.array!(@payment_requests) do |payment_request|
  json.extract! payment_request, :id, :order_no, :umrn, :txn_date, :file_name, :amount_total
  json.url payment_request_url(payment_request, format: :json)
end
