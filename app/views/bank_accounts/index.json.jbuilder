json.array!(@bank_accounts) do |bank_account|
  json.extract! bank_account, :id, :account_no, :bank_code1, :code1_type, :bank_code2, :code2_type, :bank_location
  json.url bank_account_url(bank_account, format: :json)
end
