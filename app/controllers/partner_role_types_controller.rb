class PartnerRoleTypesController < ApplicationController
  before_action :set_partner_role_type, only: [:show, :edit, :update, :destroy]

  # GET /partner_role_types
  # GET /partner_role_types.json
  def index
    @partner_role_types = PartnerRoleType.all
  end

  # GET /partner_role_types/1
  # GET /partner_role_types/1.json
  def show
  end

  # GET /partner_role_types/new
  def new
    @partner_role_type = PartnerRoleType.new
  end

  # GET /partner_role_types/1/edit
  def edit
  end

  # POST /partner_role_types
  # POST /partner_role_types.json
  def create
    @partner_role_type = PartnerRoleType.new(partner_role_type_params)

    respond_to do |format|
      if @partner_role_type.save
        format.html { redirect_to @partner_role_type, notice: 'Partner role type was successfully created.' }
        format.json { render :show, status: :created, location: @partner_role_type }
      else
        format.html { render :new }
        format.json { render json: @partner_role_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partner_role_types/1
  # PATCH/PUT /partner_role_types/1.json
  def update
    respond_to do |format|
      if @partner_role_type.update(partner_role_type_params)
        format.html { redirect_to @partner_role_type, notice: 'Partner role type was successfully updated.' }
        format.json { render :show, status: :ok, location: @partner_role_type }
      else
        format.html { render :edit }
        format.json { render json: @partner_role_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partner_role_types/1
  # DELETE /partner_role_types/1.json
  def destroy
    @partner_role_type.destroy
    respond_to do |format|
      format.html { redirect_to partner_role_types_url, notice: 'Partner role type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partner_role_type
      @partner_role_type = PartnerRoleType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partner_role_type_params
      params.require(:partner_role_type).permit(:code, :name)
    end
end
