class PaymentMandatesController < ApplicationController
  before_action :set_payment_mandate, only: [:show, :edit, :update, :destroy]

  # GET /payment_mandates
  # GET /payment_mandates.json
  def index
    @payment_mandates = PaymentMandate.all
  end

  # GET /payment_mandates/1
  # GET /payment_mandates/1.json
  def show
  end

  # GET /payment_mandates/new
  def new
    @payment_mandate = PaymentMandate.new
  end

  # GET /payment_mandates/1/edit
  def edit
  end

  # POST /payment_mandates
  # POST /payment_mandates.json
  def create
    @payment_mandate = PaymentMandate.new(payment_mandate_params)

    respond_to do |format|
      if @payment_mandate.save
        format.html { redirect_to @payment_mandate, notice: 'Payment mandate was successfully created.' }
        format.json { render :show, status: :created, location: @payment_mandate }
      else
        format.html { render :new }
        format.json { render json: @payment_mandate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payment_mandates/1
  # PATCH/PUT /payment_mandates/1.json
  def update
    respond_to do |format|
      if @payment_mandate.update(payment_mandate_params)
        format.html { redirect_to @payment_mandate, notice: 'Payment mandate was successfully updated.' }
        format.json { render :show, status: :ok, location: @payment_mandate }
      else
        format.html { render :edit }
        format.json { render json: @payment_mandate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payment_mandates/1
  # DELETE /payment_mandates/1.json
  def destroy
    @payment_mandate.destroy
    respond_to do |format|
      format.html { redirect_to payment_mandates_url, notice: 'Payment mandate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment_mandate
      @payment_mandate = PaymentMandate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_mandate_params
      params.require(:payment_mandate).permit(:mandate_no, :mandate_status, :start_date, :end_date)
    end
end
