Rails.application.routes.draw do
  resources :bank_accounts
  resources :banks
  resources :payment_mandates
  resources :payment_requests do
    member do 
      get "submit"
    end
  end
  resources :partners
  resources :partner_role_types
  #apipie
  mount Upmin::Engine => '/admin'
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
