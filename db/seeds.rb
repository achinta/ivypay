# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

bank_list = [
  "ABHYUDAYA CO-OP BANK LTD",
  "ABU DHABI COMMERCIAL BANK",
  "AKOLA DISTRICT CENTRAL CO-OPERATIVE BANK",
  "AKOLA JANATA COMMERCIAL COOPERATIVE BANK",
  "ALLAHABAD BANK",
  "ALMORA URBAN CO-OPERATIVE BANK LTD.",
  "ANDHRA BANK",
  "ANDHRA PRAGATHI GRAMEENA BANK",
  "APNA SAHAKARI BANK LTD",
  "AUSTRALIA AND NEW ZEALAND BANKING GROUP LIMITED.",
  "AXIS BANK",
  "BANK INTERNASIONAL INDONESIA",
  "BANK OF AMERICA",
  "BANK OF BAHRAIN AND KUWAIT",
  "BANK OF BARODA",
  "BANK OF CEYLON",
  "BANK OF INDIA",
  "BANK OF MAHARASHTRA",
  "BANK OF TOKYO-MITSUBISHI UFJ LTD.",
  "BARCLAYS BANK PLC",
  "BASSEIN CATHOLIC CO-OP BANK LTD",
  "BHARATIYA MAHILA BANK LIMITED",
  "BNP PARIBAS",
  "CALYON BANK",
  "CANARA BANK",
  "CAPITAL LOCAL AREA BANK LTD.",
  "CATHOLIC SYRIAN BANK LTD.",
  "CENTRAL BANK OF INDIA",
  "CHINATRUST COMMERCIAL BANK",
  "CITIBANK NA",
  "CITIZENCREDIT CO-OPERATIVE BANK LTD",
  "CITY UNION BANK LTD",
  "COMMONWEALTH BANK OF AUSTRALIA",
  "CORPORATION BANK",
  "CREDIT SUISSE AG",
  "DBS BANK LTD",
  "DENA BANK",
  "DEUTSCHE BANK",
  "DEUTSCHE SECURITIES INDIA PRIVATE LIMITED",
  "DEVELOPMENT CREDIT BANK LIMITED",
  "DHANLAXMI BANK LTD",
  "DICGC",
  "DOMBIVLI NAGARI SAHAKARI BANK LIMITED",
  "FIRSTRAND BANK LIMITED",
  "GOPINATH PATIL PARSIK JANATA SAHAKARI BANK LTD",
  "GURGAON GRAMIN BANK",
  "HDFC BANK LTD",
  "HSBC",
  "ICICI BANK LTD",
  "IDBI BANK LTD",
  "IDRBT",
  "INDIAN BANK",
  "INDIAN OVERSEAS BANK",
  "INDUSIND BANK LTD",
  "INDUSTRIAL AND COMMERCIAL BANK OF CHINA LIMITED",
  "ING VYSYA BANK LTD",
  "JALGAON JANATA SAHKARI BANK LTD",
  "JANAKALYAN SAHAKARI BANK LTD",
  "JANASEVA SAHAKARI BANK (BORIVLI) LTD",
  "JANASEVA SAHAKARI BANK LTD. PUNE",
  "JANATA SAHAKARI BANK LTD (PUNE)",
  "JPMORGAN CHASE BANK N.A",
  "KALLAPPANNA AWADE ICH JANATA S BANK",
  "KAPOL CO OP BANK",
  "KARNATAKA BANK LTD",
  "KARNATAKA VIKAS GRAMEENA BANK",
  "KARUR VYSYA BANK",
  "KOTAK MAHINDRA BANK",
  "KURMANCHAL NAGAR SAHKARI BANK LTD",
  "MAHANAGAR CO-OP BANK LTD",
  "MAHARASHTRA STATE CO OPERATIVE BANK",
  "MASHREQBANK PSC",
  "MIZUHO CORPORATE BANK LTD",
  "MUMBAI DISTRICT CENTRAL CO-OP. BANK LTD.",
  "NAGPUR NAGRIK SAHAKARI BANK LTD",
  "NATIONAL AUSTRALIA BANK",
  "NEW INDIA CO-OPERATIVE BANK LTD.",
  "NKGSB CO-OP BANK LTD",
  "NORTH MALABAR GRAMIN BANK",
  "NUTAN NAGARIK SAHAKARI BANK LTD",
  "OMAN INTERNATIONAL BANK SAOG",
  "ORIENTAL BANK OF COMMERCE",
  "PARSIK JANATA SAHAKARI BANK LTD",
  "PRATHAMA BANK",
  "PRIME CO OPERATIVE BANK LTD",
  "PUNJAB AND MAHARASHTRA CO-OP BANK LTD.",
  "PUNJAB AND SIND BANK",
  "PUNJAB NATIONAL BANK",
  "RABOBANK INTERNATIONAL (CCRB)",
  "RAJGURUNAGAR SAHAKARI BANK LTD.",
  "RAJKOT NAGARIK SAHAKARI BANK LTD",
  "RESERVE BANK OF INDIA",
  "SBERBANK",
  "SHINHAN BANK",
  "SHRI CHHATRAPATI RAJARSHI SHAHU URBAN CO-OP BANK LTD",
  "SOCIETE GENERALE",
  "SOLAPUR JANATA SAHKARI BANK LTD.SOLAPUR",
  "SOUTH INDIAN BANK",
  "STANDARD CHARTERED BANK",
  "STATE BANK OF BIKANER AND JAIPUR",
  "STATE BANK OF HYDERABAD",
  "STATE BANK OF INDIA",
  "STATE BANK OF MAURITIUS LTD",
  "STATE BANK OF MYSORE",
  "STATE BANK OF PATIALA",
  "STATE BANK OF TRAVANCORE",
  "SUMITOMO MITSUI BANKING CORPORATION",
  "SYNDICATE BANK",
  "TAMILNAD MERCANTILE BANK LTD",
  "THANE BHARAT SAHAKARI BANK LTD",
  "THE A.P. MAHESH CO-OP URBAN BANK LTD.",
  "THE AHMEDABAD MERCANTILE CO-OPERATIVE BANK LTD.",
  "THE ANDHRA PRADESH STATE COOP BANK LTD",
  "THE BANK OF NOVA SCOTIA",
  "THE BANK OF RAJASTHAN LTD",
  "THE BHARAT CO-OPERATIVE BANK (MUMBAI) LTD",
  "THE COSMOS CO-OPERATIVE BANK LTD.",
  "THE DELHI STATE COOPERATIVE BANK LTD.",
  "THE FEDERAL BANK LTD",
  "THE GADCHIROLI DISTRICT CENTRAL COOPERATIVE BANK LTD",
  "THE GREATER BOMBAY CO-OP. BANK LTD",
  "THE GUJARAT STATE CO-OPERATIVE BANK LTD",
  "THE JALGAON PEOPLES CO-OP BANK",
  "THE JAMMU AND KASHMIR BANK LTD",
  "THE KALUPUR COMMERCIAL CO. OP. BANK LTD.",
  "THE KALYAN JANATA SAHAKARI BANK LTD.",
  "THE KANGRA CENTRAL CO-OPERATIVE BANK LTD",
  "THE KANGRA COOPERATIVE BANK LTD",
  "THE KARAD URBAN CO-OP BANK LTD",
  "THE KARNATAKA STATE APEX COOP. BANK LTD.",
  "THE LAKSHMI VILAS BANK LTD",
  "THE MEHSANA URBAN COOPERATIVE BANK LTD",
  "THE MUNICIPAL CO OPERATIVE BANK LTD MUMBAI",
  "THE NAINITAL BANK LIMITED",
  "THE NASIK MERCHANTS CO-OP BANK LTD., NASHIK",
  "THE RAJASTHAN STATE COOPERATIVE BANK LTD.",
  "THE RATNAKAR BANK LTD",
  "THE ROYAL BANK OF SCOTLAND N.V",
  "THE SAHEBRAO DESHMUKH CO-OP. BANK LTD.",
  "THE SARASWAT CO-OPERATIVE BANK LTD",
  "THE SEVA VIKAS CO-OPERATIVE BANK LTD (SVB)",
  "THE SHAMRAO VITHAL CO-OPERATIVE BANK LTD",
  "THE SURAT DISTRICT CO OPERATIVE BANK LTD.",
  "THE SURAT PEOPLES CO-OP BANK LTD",
  "THE SUTEX CO.OP. BANK LTD.",
  "THE TAMILNADU STATE APEX COOPERATIVE BANK LIMITED",
  "THE THANE DISTRICT CENTRAL CO-OP BANK LTD",
  "THE THANE JANATA SAHAKARI BANK LTD",
  "THE VARACHHA CO-OP. BANK LTD.",
  "THE VISHWESHWAR SAHAKARI BANK LTD.,PUNE",
  "THE WEST BENGAL STATE COOPERATIVE BANK LTD",
  "TJSB SAHAKARI BANK LTD.",
  "TUMKUR GRAIN MERCHANTS COOPERATIVE BANK LTD.,",
  "UBS AG",
  "UCO BANK",
  "UNION BANK OF INDIA",
  "UNITED BANK OF INDIA",
  "UNITED OVERSEAS BANK",
  "VASAI VIKAS SAHAKARI BANK LTD.",
  "VIJAYA BANK",
  "WEST BENGAL STATE COOPERATIVE BANK",
  "WESTPAC BANKING CORPORATION",
  "WOORI BANK",
  "YES BANK LTD",
  "ZILA SAHKARI BANK LTD GHAZIABAD"  
]

current_banks = Bank.all
if current_banks.count == 0
  bank_list.each do |bank|
    Bank.create(name: bank)
  end
end

#################################### Partner Role Types ######################
partner_roles = [
  ["RET","Retailer"],
  ["DIS","Distributor"]
]

partner_roles.each do |partner_role|
  role = PartnerRoleType.find_by(code: partner_role[0])
  if role.nil?
    PartnerRoleType.create(code: partner_role[0],name: partner_role[1])
  end
end

#################################### Distributors ######################
distributor_list = [
  ["Western Lube"],
  ["Rocky Agencies"],
  ["Amman Distributors"],
  ["Agro Distributors"],
  ["Satvik Consultants"]
]

distributor_list.each do |dis|
  if not Partner.find_by(name: dis[0]) then
    Partner.create(name: dis[0])
  end
end
#################################### Mandates ######################
mandates = [
  #["mandate_no" , "mandate_status" , "start_date" , "end_date"   , "payer_acct_no" , "payer_bank_code" , "payer_bank_code_type" , "payee_acct_no" , "payee_bank_code" , "payee_bank_code_type" , "payer_name"    , "payee_name", "payer_mobile_no","payee_mobile_no"]       ,
  ["UNHBE54321"  , "draft"          , "2015-07-03" , "2015-09-03" , "000987678098"  , "ICIC000023"      , "IFSC"                 , "098765432112"  , "IDBI000876"      , "IFSC"                 , "Ajay Provisions" , "Western Lube","9445586145","9600132456"],
  ["XIJKI9I876"  , "draft"          , "2015-07-02" , "2015-12-03" , "987867540989"  , "IDBI000023"      , "IFSC"                 , "098768939478"  , "IDBI000349"      , "IFSC"                 , "Rajiv Grocers" , "Rocky Agencies","9600132456","9445586145"],
  ["IHNKI9I876"  , "draft"          , "2015-07-05" , "2015-12-09" , "987867598769"  , "IDBI000077"      , "IFSC"                 , "098768998760"  , "CITII00349"      , "IFSC"                 , "ALN Groceries" , "Western Lube","",""]
]

mandates.each do |new_mandate|
  mandate = PaymentMandate.find_by(mandate_no: new_mandate[0])
  if not mandate then 
    mandate_vals = {}
    mandate_keys = ["mandate_no" , "mandate_status" , "start_date" , "end_date"   , "payer_acct_no" , "payer_bank_code" , "payer_bank_code_type" , "payee_acct_no" , "payee_bank_code" , "payee_bank_code_type" , "payer_name"    , "payee_name","payee_mobile_no","payer_mobile_no"]
    mandate_keys.each_with_index do |key,index|
      mandate_vals[key] = new_mandate[index] 
    end
   
    p mandate_vals 
    PaymentMandate.create(mandate_vals)
  end
end

#################################### Payment Requests ######################
payment_requests = [
  #["UMRN","order_no","amount_total","txn_date"]
  ["XIJKI9I876","9876-0087695","3010","2015-10-7"]
]

payment_requests.each do |new_payment|
  payment = PaymentRequest.find_by(order_no: :new_payment[0])
  if not payment then
    
    payment_keys = ["umrn","order_no","amount_total","txn_date"]
    payment_vals = {} 
    payment_keys.each_with_index do |key,index|
      payment_vals[key] = new_payment[index]
    end
    PaymentRequest.create(payment_vals)
  end
end
