class MandateChanges < ActiveRecord::Migration
  def change
    change_column_null :payment_mandates, :mandate_no, false
    change_column_null :payment_mandates, :start_date, false
    add_column :payment_mandates, :src_partner_name, :string, :null => false
    add_column :payment_mandates, :src_acct_no, :string, :null => false
    add_column :payment_mandates, :src_bank_code, :string, :null => false
    add_column :payment_mandates, :src_code_type, :string, :null => false
    add_column :payment_mandates, :dst_partner_name, :string, :null => false
    add_column :payment_mandates, :dst_acct_no, :string, :null => false
    add_column :payment_mandates, :dst_bank_code, :string, :null => false
    add_column :payment_mandates, :dst_code_type, :string, :null => false

    change_column_null :payment_requests, :order_no, false
    change_column_null :payment_requests, :umrn, false
    change_column_null :payment_requests, :txn_date, false
    add_column :payment_requests, :amount_total, :decimal , :precision => 10, :scale => 2
  end
end
