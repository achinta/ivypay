class CreatePaymentMandates < ActiveRecord::Migration
  def change
    create_table :payment_mandates do |t|
      t.string :mandate_no
      t.string :mandate_status
      t.date :start_date
      t.date :end_date

      t.timestamps null: false
    end
  end
end
