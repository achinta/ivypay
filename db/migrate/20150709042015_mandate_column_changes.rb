class MandateColumnChanges < ActiveRecord::Migration
  def change
    rename_column :payment_mandates, :src_partner_name, :payer_name
    rename_column :payment_mandates, :src_acct_no, :payer_acct_no
    rename_column :payment_mandates, :src_bank_code, :payer_bank_code
    rename_column :payment_mandates, :src_code_type, :payer_bank_code_type
    add_reference :payment_mandates, :payer, references: :partners, index: true
    add_foreign_key :payment_mandates, :partners, column: :payer_id 

    rename_column :payment_mandates, :dst_partner_name, :payee_name
    rename_column :payment_mandates, :dst_acct_no, :payee_acct_no
    rename_column :payment_mandates, :dst_bank_code, :payee_bank_code
    rename_column :payment_mandates, :dst_code_type, :payee_bank_code_type
    rename_column :payment_mandates, :payee_partner_id, :payee_id
  end
end
