class PartnerColumns < ActiveRecord::Migration
  def change
    add_column :partners, :mobile_1, :string
    add_column :payment_mandates, :payer_mobile_no, :string
  end
end
