class PaymentStatus < ActiveRecord::Migration
  def change
    add_column :payment_requests, :status, :string
    add_reference :payment_requests, :payment_mandate, index: true, foreign_key: true 
  end
end
