class MandateMobileNo < ActiveRecord::Migration
  def change
    add_column :payment_mandates, :payee_mobile_no, :string
    add_reference :payment_mandates, :payee_partner, references: :partners, index: true 
    add_foreign_key :payment_mandates, :partners, column: :payee_partner_id 
  end
end
