class AddParterRoleTypeToPartner < ActiveRecord::Migration
  def change
    add_reference :partners, :partner_role_type, index: true, foreign_key: true
  end
end
