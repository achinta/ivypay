class AddBankToBankAccount < ActiveRecord::Migration
  def change
    add_reference :bank_accounts, :bank, index: true, foreign_key: true,null: false
  end
end
