class CreateBankAccounts < ActiveRecord::Migration
  def change
    create_table :bank_accounts do |t|
      t.string :account_no
      t.string :bank_code1
      t.string :code1_type
      t.string :bank_code2
      t.string :code2_type
      t.string :bank_location

      t.timestamps null: false
    end
  end
end
