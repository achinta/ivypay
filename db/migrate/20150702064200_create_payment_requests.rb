class CreatePaymentRequests < ActiveRecord::Migration
  def change
    create_table :payment_requests do |t|
      t.string :order_no
      t.string :umrn
      t.date :txn_date
      t.string :file_name

      t.timestamps null: false
    end
  end
end
