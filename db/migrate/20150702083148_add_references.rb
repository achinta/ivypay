class AddReferences < ActiveRecord::Migration
  def change
    add_reference :bank_accounts, :partner, index: true, foreign_key: true, null: false
  end
end
