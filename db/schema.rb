# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150709105119) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "account_no"
    t.string   "bank_code1"
    t.string   "code1_type"
    t.string   "bank_code2"
    t.string   "code2_type"
    t.string   "bank_location"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "bank_id",       null: false
    t.integer  "partner_id",    null: false
  end

  add_index "bank_accounts", ["bank_id"], name: "index_bank_accounts_on_bank_id", using: :btree
  add_index "bank_accounts", ["partner_id"], name: "index_bank_accounts_on_partner_id", using: :btree

  create_table "banks", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partner_role_types", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "partner_role_type_id"
    t.string   "mobile_1"
  end

  add_index "partners", ["partner_role_type_id"], name: "index_partners_on_partner_role_type_id", using: :btree

  create_table "payment_mandates", force: :cascade do |t|
    t.string   "mandate_no",           null: false
    t.string   "mandate_status"
    t.date     "start_date",           null: false
    t.date     "end_date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "payer_name",           null: false
    t.string   "payer_acct_no",        null: false
    t.string   "payer_bank_code",      null: false
    t.string   "payer_bank_code_type", null: false
    t.string   "payee_name",           null: false
    t.string   "payee_acct_no",        null: false
    t.string   "payee_bank_code",      null: false
    t.string   "payee_bank_code_type", null: false
    t.string   "payee_mobile_no"
    t.integer  "payee_id"
    t.integer  "payer_id"
    t.string   "payer_mobile_no"
  end

  add_index "payment_mandates", ["payee_id"], name: "index_payment_mandates_on_payee_id", using: :btree
  add_index "payment_mandates", ["payer_id"], name: "index_payment_mandates_on_payer_id", using: :btree

  create_table "payment_requests", force: :cascade do |t|
    t.string   "order_no",                                    null: false
    t.string   "umrn",                                        null: false
    t.date     "txn_date",                                    null: false
    t.string   "file_name"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.decimal  "amount_total",       precision: 10, scale: 2
    t.string   "status"
    t.integer  "payment_mandate_id"
  end

  add_index "payment_requests", ["payment_mandate_id"], name: "index_payment_requests_on_payment_mandate_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "bank_accounts", "banks"
  add_foreign_key "bank_accounts", "partners"
  add_foreign_key "partners", "partner_role_types"
  add_foreign_key "payment_mandates", "partners", column: "payee_id"
  add_foreign_key "payment_mandates", "partners", column: "payer_id"
  add_foreign_key "payment_requests", "payment_mandates"
end
